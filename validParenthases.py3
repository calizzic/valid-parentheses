class Solution:
    def isValid(self, s: str) -> bool:
        vals = []
        for c in s:
            if c==')' or c == '}' or c == ']':
                if len(vals)>0:
                    if vals[len(vals)-1] != c:
                        return False
                    else:
                        vals.pop()
                else:
                    return False
            else:
                match c:
                    case '(':
                        vals.append(')')
                    case '[':
                        vals.append(']')
                    case '{':
                        vals.append('}')
        return len(vals)==0